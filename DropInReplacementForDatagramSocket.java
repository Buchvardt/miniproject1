import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
public class DropInReplacementForDatagramSocket extends DatagramSocket{

	//percentage to corrupt
	private double percentage;
	private int dCorruptionType;

	//constructor that takes percentages to discard of sent datagrams
	public DropInReplacementForDatagramSocket(int corruptPercentage, int desiredCorruptionType) throws SocketException{
		double tmp = (double) corruptPercentage/100;
		percentage = tmp;
		dCorruptionType = desiredCorruptionType;
	}

	//Override super send method
	public void send(DatagramPacket p) throws IOException{
			
			DatagramPacket tmp = randomCorrupt(p);
			super.send(tmp);
	}

	private DatagramPacket randomCorrupt(DatagramPacket p){

		//print empty line to console
		System.out.println();

		//Retrieve the data
		byte[] originalData = p.getData();

		//random number 1, 2 or 3 for the corruptiontype
		int corruptType = randInt(1, 3);

		if ( 0 < dCorruptionType && dCorruptionType < 4 ){
			corruptType = dCorruptionType;
		}

			DatagramPacket corruptedDatagramPacket = p;
        	byte[] corruptedData;
        	ArrayList<Integer> randomIndecies = null;
        	int corruptetIndex = 0; 
        	int corruptedLength = 0;       

        switch (corruptType){ 
        	

           	//discard
            case 1: 
            	System.out.println("DISCARD");
            		//Find the length of the corruptet byte[]
					corruptedLength = (int) (percentage * originalData.length);

					//creta the corruptet data byte[]
            		corruptedData = new byte[originalData.length - corruptedLength];

            		//Find random indecies for the bytes that needs to be discarded
            		randomIndecies = uniqueRandomNumbers(p.getLength(), corruptedLength);

					// discard the random data
					corruptetIndex = 0;
					for(int i=0; i<originalData.length; i++){
						if(!randomIndecies.contains(i)){
							corruptedData[corruptetIndex] = originalData[i];
							corruptetIndex++;
						}
					}
               		System.out.println("Indecies to be discarded" + randomIndecies.toString());
					System.out.println("discard Percentage " + percentage);
					System.out.println("data length " + p.getLength()); 
					System.out.println("corruptedLength " + corruptedLength);

					corruptedDatagramPacket = new DatagramPacket(corruptedData, corruptedLength, p.getAddress(), p.getPort());
                    break;

            //duplicate
            case 2: 
            		System.out.println("DUPLICATE");
            		//Find the length, (number of bytes), that gets corrupted
					corruptedLength = (int) (percentage * p.getLength());

					//Find random indecies for the bytes that need to be duplicatet
            		randomIndecies = uniqueRandomNumbers(p.getLength(), corruptedLength);

            		//create the corruptet data byte[]
            		corruptedData = new byte[p.getLength() + corruptedLength];

            		
            		//duplicate the random indecies
            		corruptetIndex = 0;
            		for(int i = 0; i < originalData.length; i++){

            			if (randomIndecies.contains(i)){ // make an ekstra copy
            				corruptedData[corruptetIndex] = originalData[i];
            				corruptetIndex++;
            				corruptedData[corruptetIndex] = originalData[i];
            				corruptetIndex++;
               			}
            			else{ // just transfer the data
               				corruptedData[corruptetIndex] = originalData[i];
               				corruptetIndex++;
               			}
            		}

               		System.out.println("Indecies to be copied" + randomIndecies.toString());
	           		System.out.println("discard Percentage " + percentage);
					System.out.println("data length " + originalData.length); 
					System.out.println("corrupted Length " + corruptedData.length);

					corruptedDatagramPacket = new DatagramPacket(corruptedData, corruptedData.length, p.getAddress(), p.getPort());
                    break;
            //reorder
            case 3:	
            		System.out.println("REORDER");
            		//Find the length, (number of bytes), that gets corrupted
					corruptedLength = (int) (percentage * p.getLength());

					//Find random indecies for the bytes that need to be reordered
            		randomIndecies = uniqueRandomNumbers(p.getLength(), corruptedLength);

            		//shuffle the random indicies
            		ArrayList<Integer> shuffledRandomIndecies = new ArrayList<Integer>();
            		for(int i = 0; i < randomIndecies.size(); i++){
            			shuffledRandomIndecies.add(randomIndecies.get(i));
            		}
            		Collections.shuffle(shuffledRandomIndecies);

            		System.out.println("Indecies to be reordered " + randomIndecies.toString());
            		System.out.println("The reordered indecies " + shuffledRandomIndecies.toString());

            		//create the corruptet data byte[]
            		corruptedData = new byte[originalData.length];

            		//construct the corruptet data
            		for(int i = 0; i < originalData.length; i++){

            			if (randomIndecies.contains(i)){
            				// "pop" the last element off the shuffledRandomIndecies list 
            				int tmpIndex = shuffledRandomIndecies.get(shuffledRandomIndecies.size() - 1);
            				shuffledRandomIndecies.remove(shuffledRandomIndecies.size() -1);

            				//place data from this index from the original data,  in index i in the corrupted data
            				corruptedData[i] = originalData[tmpIndex];
               			}
            			else{ // just transfer the data
               				corruptedData[i] = originalData[i];
               			}

            		}
            		
	           		System.out.println("reorder Percentage " + percentage);
					System.out.println("data length " + originalData.length); 
					System.out.println("corrupted Length " + corruptedData.length);

					corruptedDatagramPacket = new DatagramPacket(corruptedData, corruptedData.length, p.getAddress(), p.getPort());
            		break;
        }
	
	//print empty line to console
	System.out.println();
	return corruptedDatagramPacket;
	}

	private ArrayList<Integer> uniqueRandomNumbers(int originalLength, int reducedLength){

		ArrayList<Integer> dummyArray = new ArrayList<Integer>();

		for (int i = 0; i < originalLength; i++){
			dummyArray.add(i);
		}

		Collections.shuffle(dummyArray);

		ArrayList<Integer> reducedArray = new ArrayList<Integer>();
		for(int i = 0; i < reducedLength; i++){
			reducedArray.add(dummyArray.get(i));
		}

		Collections.sort(reducedArray);

		return reducedArray;
	}

	private static int randInt(int min, int max){
		Random rand = new Random();

		int randomNum = rand.nextInt((max - min) +1) + min;

		return randomNum;
	}
}