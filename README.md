# README #

Copy the folder to a desired repository.
Compile the 3 .java files.
  
### What is this repository for? ###

* Quick summary
This is a UDP client/server java program, that kan simulate a desired percentage of corruption in the dataPacket sendt via the modified socket.

The corruption is randomly set to either DISCARD, DUPLICATE or REORDERING.

For testing purposes the type of corruption can by chosen manually.
 
* Version
1

### How do I get set up? ###

* Deployment instructions

Run UDPServer.java in the terminal.

Then run the UDPClient.java in another terminal with the following args[]
args[0] message
args[1] host
args[2] corruption percentage
args[3] is optional. Type 1 for DISCARD, 2 for UDPLICATE or 3 for REORDER

ex:

java UDPClient "Hello from client" "localhost" "30" "1"

### Contribution guidelines ###

* Code review
Please suggest improvements to methods and syntax

* Other guidelines
This is one interpretation of corrupting a set percentage of a datagram. 
The corruption is chosen pr byte. 

i.e if the message is "1234" and the percentage is "50" the reply from the server will corrupt two bytes. 
This means that: 
DISCARD will return 2 bytes eg "12", 
DUPLICATE will return 6 bytes eg "112334" and
REORDER will return 4 bytes eg "1432" where two bytes are reordered.